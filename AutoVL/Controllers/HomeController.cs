﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace AutoVL.Controllers
{
    public class HomeController : Controller
    {
       public static int flag2 = 0;
        public ActionResult Index()
        {
            if (flag2 == 0)
            {
                NumberModel.Lst55 = List55();
                flag2 = 1;
            }
            else
            {
                Task t2 = Task.Factory.StartNew(hhhh);
            }

            return View();
        }
        public void hhhh()
        {
            while (true)
            {
                try
                {
                    if (DateTime.Now.Hour == 1)
                    {
                        if (NumberModel.date == DateTime.MinValue || NumberModel.date != DateTime.Now.Date)
                        {
                            NumberModel.Lst55 = List55();
                        }
                    }
                    Thread.Sleep(30000);
                }
                catch (Exception ex)
                {

                    throw;
                }
            }
        }
        public JsonResult Getnumber()
        {

            DateTime dt = DateTime.Now.Date;
            NumberModel obj = new NumberModel();
            GetNumber getnumber = new GetNumber();
            var str1 = Random45();
            var str2 = Random55();
            var str3 = DeAndLo();
            var str4 = DeAndLo();

            if (NumberModel.date == null || NumberModel.date.ToString("dd/MM/yyyy") == "01/01/0001" || NumberModel.date != dt.Date)
            {
                NumberModel.date = dt.Date;
                NumberModel.VL45 = str1;
                NumberModel.VL55 = str2;
                NumberModel.De = str3;
                NumberModel.Lo = str4;

                getnumber.VL45 = NumberModel.VL45;
                getnumber.VL55 = NumberModel.VL55;
                getnumber.De = NumberModel.De;
                getnumber.Lo = NumberModel.Lo;

                return Json(getnumber);
            }

            getnumber.VL45 = NumberModel.VL45;
            getnumber.VL55 = NumberModel.VL55;
            getnumber.De = NumberModel.De;
            getnumber.Lo = NumberModel.Lo;
            return Json(getnumber);
        }
        public string Random45()
        {
            string str = string.Empty;
            List<NumberModel> lstRandom45 = new List<NumberModel>();
            while (lstRandom45.Count() < 6)
            {
                int number = 0;
                Random rd = new Random();
                number = rd.Next(1, 46);
                if (!lstRandom45.Any(m => m.Number == number))
                {
                    var obj = new NumberModel();
                    obj.Number = number;
                    lstRandom45.Add(obj);
                }
            }
            var lstString = lstRandom45.Select(m => m.Number);
            str = string.Join(",", lstString.ToArray());
            return str;
        }
        public string Random55()
        {
            string str = string.Empty;
            List<NumberModel> lstnumber55 = new List<NumberModel>();
            var lst55 = NumberModel.Lst55;

            while (lstnumber55.Count < 6)
            {
                int number = 0;
                Random rd = new Random();
                number = rd.Next(1, 56);

                if (!lst55.Contains(number.ToString()))
                {
                    if (!lstnumber55.Any(m => m.Number == number))
                    {
                        var obj = new NumberModel();
                        obj.Number = number;
                        lstnumber55.Add(obj);
                    }
                }
            }
            var lstString = lstnumber55.Select(m => m.Number);
            str = string.Join(",", lstString.ToArray());
            return str;
        }
        public string DeAndLo()
        {
            string str = string.Empty;
            Random rd = new Random();
            str = rd.Next(100).ToString();
            return str;
        }

        public JsonResult ChangeGetNumber(int values)
        {
            string str = string.Empty;
            if (values == 1)
            {
                str = DeAndLo();
            }
            else if (values == 2)
            {
                str = DeAndLo();
            }
            else if (values == 3)
            {
                str = Random45();
            }
            else
            {
                str = Random55();
            }
            return Json(str);
        }
        public class NumberModel
        {
            public int Number { get; set; }
            public static string De { get; set; }
            public static string Lo { get; set; }
            public static string VL55 { get; set; }
            public static string VL45 { get; set; }
            public static DateTime date { get; set; }
            public static List<string> Lst55 { get; set; }
        }
        public class GetNumber
        {

            public string De { get; set; }
            public string Lo { get; set; }
            public string VL55 { get; set; }
            public string VL45 { get; set; }

        }
        public List<string> List55()
        {
            List<string> lst55 = new List<string>();
            IWebDriver driver = new ChromeDriver();
            INavigation nav = driver.Navigate();
            nav.GoToUrl("https://vietlott.vn/vi/trung-thuong/ket-qua-trung-thuong/655.html");
            var str = driver.FindElement(By.ClassName("day_so_ket_qua_v2")).Text;
            driver.Quit();
            str = str.Replace("|", "");
            string flag = "";
            for (int j = 0; j < str.Length; j++)
            {
                flag += str[j];
                if (flag.Length == 2)
                {
                    lst55.Add(flag);
                    flag = "";
                }
            }

            return lst55;
        }

    }
}